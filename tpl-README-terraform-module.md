# tf-module-xyz

This module creates ...

## Usage

In a terraform file (.tf) we should set the declaration of the module with the variables like this:

```hcl
module "xyz" {
  source             = "git::https://host/modules/tf-module-xyz.git?ref=master"
  project            = var.project
  cluster_name       = var.cluster_name
  location           = var.location
}
```

**NOTE:** Please see [below](#variables) for information on each configuration option.

## Variables

For more info, please see the [variables file](variables.tf).

### Required Variables

| Variable                 | Description                        |
| :----------------------- | :----------------------------------|
| `project` | The ID of the project in which the resource belongs. |
| `cluster_name` | The cluster name. |
| `location` | The location (region or zone) in which the cluster master will be created. |

### Optional Variables

| Variable               | Description                         | Default                                               |
| :--------------------- | :---------------------------------- | :---------------------------------------------------- |
| `gke_disabled_http_load_balancing` | The status of the HTTP (L7) load balancing controller addon, which makes it easy to set up HTTP load balancers for services in a cluster. | true |
| `gke_disabled_horizontal_pod_autoscaling`| The status of the Horizontal Pod Autoscaling addon. | false |


### Output Variables

| Variable                 | Description                        |
| :----------------------- | :----------------------------------|
| `endpoint` | The IP address of this cluster's Kubernetes master. |
| `instance_group_urls` | List of instance group URLs which have been assigned to the cluster. |
| `master_version` |  The current version of the master in the cluster. This may be different than the min_master_version set in the config if the master has been updated by GKE. |

## Links

- https://www.terraform.io/docs/providers/google/r/container_node_pool.html
