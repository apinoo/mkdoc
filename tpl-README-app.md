# app

This is a sample application ...

## Requirements

The requirements are:

- a.
- b.
- c.

## Building

To build the application, you must have Go already installed or build the
Dockerfile. We assume that you have your Go installation working.

### Building the app

To build the app ...

### Build the Dockerfile

To build the Dockerfile ...

```bash
docker build -t app:0.0.1 .
```

## Running

### Run the app

Execute the ...

```bash
$ ./app
2020/02/10 12:34:56 Listening port: 8080
```

### Run the docker image

Execute docker run with the port 8080

```bash
$ docker run -it --rm --name app -p 8080:8080 app:0.0.1
2020/02/10 12:34:56 Listening port: 8080
```

## Access to the sample application

To access the application ...

```bash
$ curl http://localhost:8080/
```
