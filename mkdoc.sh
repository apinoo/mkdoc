#!/bin/bash

MKDOC=$HOME/bin/mkdoc
TEMPLATES_TYPES=(
  "gitlab-ci"
  "dockerfile"
  "app"
  "terraform-module"
)

die() {
  printf '%s\n' "$1" >&2
  exit 1
}

usage() {
    echo "Usage: $0 [options] path" 1>&2
    echo "Options:"
    echo "-h|-\?|--help: Help info."
    echo "-l|--list: List available types."
    echo "-t|--type: Create doc with type."
    exit
}

path=
type=

validate_readme() {

  local counter_types=1
  if [ -z "$1" ]; then
    echo "Readme type is required."
    exit 1
  fi

  for type in ${TEMPLATES_TYPES[@]}; do
    if [ "$type" = "$1" ]; then
      echo "Type to create $type"
      break
    fi
    if [ "$counter_types" -eq ${#TEMPLATES_TYPES[@]} ]; then
      echo "Type is invalid."
      exit 1
    fi
    counter_types=$(( counter_types + 1 ))
  done
}

get_path() {
  if [ -z "$1" ]; then
    to_path="."
  else
    to_path="$1"
  fi 

  if [ "${to_path: -1}" = "/" ]; then
    to_path="${to_path%/}"
  fi
  echo "to_path: ${to_path}"
}

create_files() {
  echo "Creating $MKDOC/tpl-README-$type.md to $to_path/README.md"
  cp $MKDOC/tpl-README-$type.md $to_path/README.md
  if [ "$?" -eq 0 ]; then
    echo "Done."
  else
    echo "Fail."
    exit 1
  fi

  echo "Creating $MKDOC/tpl-CHANGELOG.md to $to_path/CHANGELOG.md"
  cp $MKDOC/tpl-CHANGELOG.md $to_path/CHANGELOG.md
  if [ "$?" -eq 0 ]; then
    echo "Done."
  else
    echo "Fail."
    exit 1
  fi
}

list_types() {
  echo "Available types:"
  for array_type in ${TEMPLATES_TYPES[@]}; do
    echo "  ${array_type}"
  done
}

while :; do
  case $1 in

    -h|-\?|--help)
      usage
      exit
      ;;

    -l|--list)
      list_types
      exit
      ;;

    -t|--type)

      if [ "$2" ]; then
        type="$2"
        shift
      else
        die '"-t|--type" requires a path option argument.'
      fi
      validate_readme $1
      get_path $2
      create_files
      exit
      ;;

    -v|--verbose)
      verbose=$((verbose + 1))
      ;;

    --)
      shift
      break
      ;;

    -?*)
      printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
      ;;

    *)
      usage
      break
  esac
  shift
done
