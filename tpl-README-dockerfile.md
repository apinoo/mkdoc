# {{dockerfile}}

Dockerfile to create an image with ....

## Usage

This repository automatically builds containers for using the ...
command line program.

## Example

```bash
docker run -i -t registry.gitlab.com/apino-docker/...
```

## Links

- <https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/>