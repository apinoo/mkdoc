#!/bin/bash

if [ ! -d "$HOME/bin" ]; then
  echo "$HOME/bin doesn't exist"
  exit 1
fi

MKDOC_PATH=$HOME/bin/mkdoc
if [ ! -d "$MKDOC_PATH" ]; then
  echo "Creating $MKDOC_PATH"
  mkdir $MKDOC_PATH
fi
echo "Installing ..."
cp mkdoc.sh $MKDOC_PATH/
cp *.md $MKDOC_PATH/
echo "Done."
echo "#######################################"
echo "Please set your PATH with $MKDOC_PATH"
echo "PATH=\"\$PATH:\$HOME/bin:\$HOME/bin/mkdoc\""
