# title

Gitlab-CI yaml to ...

## Usage

Include the `gitlab-ci-template.yml` of this project into the `.gitlab-ci.yml`
of the project in which you want to use.

## Example

```yaml
include:
  - project: 'path¡
    ref: master
    file: 'gitlab-ci-template.yml'

stages:
  - lint

job:
  extends: .job
```

## Jobs

### List

The jobs available in this template.

| Name         | Stage    | Description                  |
| :----------- | :------- | :--------------------------- |
| `.job` | lint     | Job description. |

### Variables

| Variable | Required | Scope | Description | Default |
| :------- | :------- | :---- | :-----------| :------ |
| `SOME_VARIABLE` | No | .job | Var description. | default-value |

## Links

- <https://docs.gitlab.com/ee/ci/yaml/>
